using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StacjaIt.MyVod.Domain.Movies;

namespace StacjaIt.MyVod.Infrastructure
{
    internal class CastMemberEntityTypeConfiguration : IEntityTypeConfiguration<CastMember>
    {
        public void Configure(EntityTypeBuilder<CastMember> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id)
                .HasConversion(x => x.Value, x => new CastMemberId(x));

            builder.Ignore(x => x.DomainEvents);

            builder.Property(x => x.FirstName).IsRequired()
                .HasMaxLength(100);
            builder.Property(x => x.LastName).IsRequired();

            builder.ToTable("CastMembers", MoviesContext.Schema);
        }
    }
}