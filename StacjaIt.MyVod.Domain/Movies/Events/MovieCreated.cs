using MediatR;

namespace StacjaIt.MyVod.Domain.Movies.Events
{
    public class MovieCreated : INotification
    {
        public Movie Movie { get; set; }

        public MovieCreated(Movie movie)
        {
            Movie = movie;
        }
    }
}