using System;
using StacjaIt.MyVod.SharedKernel;

namespace StacjaIt.MyVod.Domain.Movies
{
    public sealed class CastMember : Entity<CastMemberId>
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }

        [Obsolete("Only For EFCore", true)]
        public CastMember()
        {
        }

        public CastMember(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
            
            Id = CastMemberId.New();
        }
    }
}