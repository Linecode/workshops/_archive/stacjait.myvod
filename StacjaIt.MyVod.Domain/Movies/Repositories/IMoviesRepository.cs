using StacjaIt.MyVod.SharedKernel;

namespace StacjaIt.MyVod.Domain.Movies.Repositories
{
    public interface IMoviesRepository
    {
        IUnitOfWork UnitOfWork { get; }
        void Add(Movie movie);
    }
}